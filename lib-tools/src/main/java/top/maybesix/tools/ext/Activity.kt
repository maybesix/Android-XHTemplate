package top.maybesix.tools.ext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import top.maybesix.tools.utils.IntentUtils

/**
 * author：  HyZhan
 * create：  2019/8/1
 * desc：    TODO
 */
inline fun <reified T : Activity> Context.startActivity(extras: Bundle) {
    val intent = Intent(this, T::class.java)
    intent.putExtras(extras)
    this.startActivity(intent)
}

inline fun <reified T : Activity> Context.startActivity(intent: Intent) {

    this.startActivity(intent)
}

inline fun <reified T : Activity> Context.startActivity() {
    val intent = Intent(this, T::class.java)
    this.startActivity(intent)
}

inline fun <reified T : Activity> Fragment.startActivity(vararg params: Pair<String, Any?>) {
    val intent = Intent(context, T::class.java)
    if (params.isNotEmpty()) IntentUtils.fillIntentArguments(intent, params)
    activity?.startActivity(intent)
}