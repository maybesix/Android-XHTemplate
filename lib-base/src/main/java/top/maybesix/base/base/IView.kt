package top.maybesix.base.base

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc 将代码初始化的分类.
 */

interface IView {

    // 获取布局id
    fun getLayoutId(): Int

    // 初始化view之前做的一操作
    fun initBefore() {}

    // 初始化控件
    fun initView()

    // 初始化事件
    fun initEvent() {}

    // 初始化适配器
    fun initAdapter() {}

    // 初始化监听事件
    fun initListener() {}

    // 初始化数据
    fun initData()
}