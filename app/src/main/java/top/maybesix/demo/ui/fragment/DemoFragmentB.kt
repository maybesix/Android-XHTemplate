package top.maybesix.demo.ui.fragment

import top.maybesix.demo.R
import top.maybesix.demo.common.MyFragment
import top.maybesix.demo.ui.activity.HomeActivity

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class DemoFragmentB : MyFragment<HomeActivity>() {

    companion object {

        fun newInstance(): DemoFragmentB {
            return DemoFragmentB()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_demo_b
    }

    override fun initView() {

    }

    override fun initData() {

    }


}