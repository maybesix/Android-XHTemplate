package top.maybesix.demo.ui.copy

import top.maybesix.demo.R
import top.maybesix.demo.common.MyFragment

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
class CopyFragment : MyFragment<CopyActivity>() {

    companion object {

        fun newInstance(): CopyFragment {
            return CopyFragment()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_copy
    }

    override fun initView() {

    }

    override fun initData() {

    }


}