package top.maybesix.demo.ui.fragment

import top.maybesix.demo.R
import top.maybesix.demo.common.MyFragment
import top.maybesix.demo.ui.activity.HomeActivity

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class DemoFragmentC : MyFragment<HomeActivity>() {

    companion object {

        fun newInstance(): DemoFragmentC {
            return DemoFragmentC()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_demo_c
    }

    override fun initView() {

    }

    override fun initData() {

    }


}