package top.maybesix.demo.common

import android.view.ViewGroup
import com.gyf.immersionbar.ImmersionBar
import com.hjq.bar.TitleBar
import top.maybesix.base.base.BaseFragment
import top.maybesix.demo.action.TitleBarAction

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
abstract class MyFragment<A : MyActivity> : BaseFragment<A>(),
    TitleBarAction {
    /** 标题栏对象  */
    private var mTitleBar: TitleBar? = null

    /** 状态栏沉浸  */
    private var mImmersionBar: ImmersionBar? = null

    override fun initFragment() {
        titleBar?.setOnTitleBarListener(this)

        initImmersion()
        super.initFragment()
    }

    /**
     * 初始化沉浸式
     */
    protected open fun initImmersion() {

        // 初始化沉浸式状态栏
        if (isStatusBarEnabled()) {
            statusBarConfig()?.init()

            // 设置标题栏沉浸
            mTitleBar?.let {
                ImmersionBar.setTitleBar(this, mTitleBar)
            }

        }
    }

    /**
     * 是否在Fragment使用沉浸式
     */
    open fun isStatusBarEnabled(): Boolean {
        return false
    }

    /**
     * 获取状态栏沉浸的配置对象
     */
    protected open fun getStatusBarConfig(): ImmersionBar? {
        return mImmersionBar
    }

    /**
     * 初始化沉浸式
     */
    private fun statusBarConfig(): ImmersionBar? {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this) // 默认状态栏字体颜色为黑色
            .statusBarDarkFont(statusBarDarkFont()) // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .keyboardEnable(true)
        return mImmersionBar
    }

    /**
     * 获取状态栏字体颜色
     */
    protected open fun statusBarDarkFont(): Boolean {
        // 返回真表示黑色字体
        return false
    }

    override fun getTitleBar(): TitleBar? {
        if (mTitleBar == null) {
            mTitleBar = findTitleBar(view as ViewGroup)
        }
        return mTitleBar
    }

    fun toast() {

    }

    override fun onResume() {
        super.onResume()
        // 重新初始化状态栏
        statusBarConfig()?.init()
    }

}