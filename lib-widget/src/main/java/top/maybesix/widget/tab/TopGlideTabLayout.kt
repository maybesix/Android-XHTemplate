package top.maybesix.widget.tab

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class TopGlideTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val mContext: Context = context

}