package top.maybesix.widget.tab

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.LinearLayout

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class BottomTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val mContext: Context
    private val mTabsContainer: LinearLayout

    init {
        //重写onDraw方法,需要调用这个方法来清除flag
        setWillNotDraw(false)
        //该属性值设置为false以确保该元素可以超出边界。缺省值为true，也即子元素不可以超出父元素的边界
        clipChildren = false
        //设置false忽略padding
        clipToPadding = false
        mContext = context
        mTabsContainer = LinearLayout(context)
        addView(mTabsContainer)
        //设置xml映射
//        obtainAttributes(context, attrs)

        //get layout_height
        val height =
            attrs!!.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height")

        //create ViewPager
//        if (height == ViewGroup.LayoutParams.MATCH_PARENT.toString()) {
//        } else if (height == ViewGroup.LayoutParams.WRAP_CONTENT.toString()) {
//        } else {
//            val systemAttrs = intArrayOf(android.R.attr.layout_height)
//            val a = context.obtainStyledAttributes(attrs, systemAttrs)
//            mHeight = a.getDimensionPixelSize(0, ViewGroup.LayoutParams.WRAP_CONTENT)
//            a.recycle()
//        }
//        mValueAnimator = ValueAnimator.ofObject(PointEvaluator(), mLastP, mCurrentP)
//        mValueAnimator.addUpdateListener(this)
    }


}